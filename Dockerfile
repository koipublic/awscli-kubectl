FROM alpine:3.11

ARG VCS_REF
ARG BUILD_DATE

# Metadata
LABEL org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.name="awscli-kubectl" \
      org.label-schema.url="https://hub.docker.com/r/vivekpd15/awscli-kubectl/" \
      org.label-schema.vcs-url="https://gitlab.com/koipublic/awscli-kubectl" \
      org.label-schema.build-date=$BUILD_DATE

ARG AWS_VERSION="1.18.124"
ARG KUBE_VERSION="v1.18.8"

RUN apk update \
      &&  apk add --no-cache ca-certificates curl py-pip py2-pip \
      &&  pip install --no-cache --upgrade pip "awscli==${AWS_VERSION}" \
      &&  curl --silent -L https://storage.googleapis.com/kubernetes-release/release/${KUBE_VERSION}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl \
      &&  chmod +x /usr/local/bin/kubectl \
      &&  curl --silent -L curl -o aws-iam-authenticator https://amazon-eks.s3.us-west-2.amazonaws.com/1.17.9/2020-08-04/bin/linux/amd64/aws-iam-authenticator -o /usr/local/bin/aws-iam-authenticator \
      &&  chmod +x /usr/local/bin/aws-iam-authenticator \
      &&  rm /var/cache/apk/*
